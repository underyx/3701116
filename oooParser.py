# This Python file uses the following encoding: utf8
import sys
import re

def oooParser(filename):
  text = [re.sub("[/\t].+","",line) for line in open(filename).read().splitlines()]
  return text

def androidWriter(filename,outtext):
  outfile = open(filename, 'w')
  outfile.write("|".join(outtext))

def main():
  if len(sys.argv) != 3:
    print 'usage: ./dictconv.py file-to-read file-to-write'
    sys.exit(1)
  
  outtext = oooParser(sys.argv[1])
  androidWriter(sys.argv[2],outtext)

if __name__ == '__main__':
  main()